#  Writing segments to disk
## Using transactions and commits to prevent corruption

In a filesystem there's some concerns regarding robustness. For instance, it
needs to ensure that all blocks marked as free are actually free; that inodes
don't point to dead blocks; and that the checkpoint inode map stays valid.

This can be achieved with some careful order of operations around writes and
persistence to disk, and policies on when the checkpoint and free block vectors
are flushed to disk.

By default, LLFS wants to keep a fixed size inode map of 2 blocks at the end of
each segment. When the segment is full (`N_BLOCKS_PER_SEGMENT - 2` blocks are
written) then the FBV is updated to reflect the in-memory FBV and values in the
superblock are also updated to point to new locations for the inode map
checkpoint and segment write head.

However, this presents an issue for writes near the end of a segment and ones
that are larger than a segment (or multiple).

In these drawings, a segment is 10 blocks (characters) wide. `XX` is the inode
map and `+` is a written data block, while `I` is the inode (block-wide).
Consider single and double indirect blocks as just data blocks too.

Imagine this layout:

```
In memory: [+++I    XX]
On disk:   [II+++I+IXX][++I++++IXX][          ][          ][          ]
```

This disk has had two rare perfectly fitting segments, where there's no wasted
space and the algorithm can place the map (XX) in no problem. Often things won't
line up this way, and you'll end up with `[++++I++I XX]` in memory where isn't
not possible to fit even the smallest file (data block+inode). The wasted space
is only one block, that's fine, nothing to lose sleep over. For the next write,
just start a new segment, right?

But what if there's 2 blocks free. Or 3. Maybe if a small file is written it'll
fit but not if it's a large one. Deciding to just move onto the next one becomes
a lot of wasted space depending on the size of the next file. Then again, not
moving on means writing a segment with data blocks that don't have their inode
in the same segment. That's dangerous - if the filesystem crashes then the FBV
will mark those as used but there's no inode to point to them. Furthermore,
wasting space aside, you can't always just up and move to the next segment when
there's no room to fit all data blocks and the inode together with the map.
There _will be many_ files that are multiple segments long. How is that handled?

Let's say we continue from the above diagram, but start writing the data blocks
of a big file, and for now dismiss the corruption that could come about from
writing data without an inode or map: 

```
In memory: [+++I++++XX]
                ^^^^ These 4 don't have an inode yet
On disk:   [II+++I+IXX][++I++++IXX][          ][          ][          ]
```
\*Write segment*
```
In memory: [++++++++XX]
On disk:   [II+++I+IXX][++I++++IXX][+++I++++XX][          ][          ]
```
\*Write segment*
```
In memory: [+++++I  XX]
On disk:   [II+++I+IXX][++I++++IXX][+++I++++XX][++++++++XX][          ]
```

There's no reason to write the last segment since it's not full. Dangerous
though! Look at all those data blocks that might never have their inode written
if crash happened right now.

Also, another thing, look at the last two maps written to disk. What's different
between them? Nothing. There's no reason to write a map if no inode has been
written at all in a segment...

How could this be improved?

I should mention that inode maps should never split across segments. That's bad
and could lead to a completely corrupt filesystem if the other half doesn't
write. I don't consider it a solution to the problem of wasting space.

Consider a _"commit"_ to mean an inode map has been written which secures all
inodes and their blocks before it.

To avoid having the FBV contain blocks that aren't yet commited, it could only
be written _with_ the map. However, this has issues with determining the next
free segment, as well as the segment cleaner. Both because they use the FBV and
will treat those pending data blocks as free in their calculations. Bad. The
solution is to have them use an in-memory FBV and not persist the one on disk
until the map is also persisted. That way in a crash all the data blocks are
lost and that's fine. Unfortunately it means you can lose more than one segment,
which isn't what log structure file systems promised.

Back to how we can improve by not pointlessly write inode maps, consider this
example memory segment `[++++I   XX]`. Now, write 14 blocks to it. There's no
way I'd fit them and their inode, of course. To avoid pointlessly writing maps,
can we not write this one right now? Maybe just do it later?

No. That doesn't work because that logic let's the inode map keep getting pushed
further and further back putting more writes at risk of being lost. Let's say,
_since there's an inode, it needs to be commited_.

As for the large 14 block file, it has 3 block written to memory and then disk,
but not committed. During the commit, the FBV is written, but ignores the 3
blocks like this:

```
In memory: [        XX] (New segment)
On disk:   [++++I+++XX]
                 ^^^ Not committed
```

Continue writing the data blocks... `[+++++++XX]` Does it need an inode map? No,
there's no inode written. _Fill with entire data blocks_. Write it to disk:

```
In memory: [++I     XX]
             ^ Single indirection block since > 10 blocks
On disk:   [++++I+++XX][++++++++++]
                 ^^^    ^^^^^^^^^^ Not committed
```

It stopped because the block isn't full. Now, that's a lot on the line! Let's
write it early to disk. What about the wasted space though? That's most of the
segment unused.

Well, there's already a procedure used for `llfs_vdisk_close()` that writes a
segment and checkpoints the map and FBV, but, marks it for restoration. In this
case, it means writing to disk as a safety measure for crashing, but keeping the
segment in memory and planning to write the rest of the segment (from after the
_I_) later once full.

Notice here that everthing is committed:

```
In memory: [++I     XX] (Written but left in memory!)
               ^ Segment read head for the write when its full
On disk:   [++++I+++XX][++++++++++][++I     XX]
                                       ^ Segment write head
```

Best of both worlds, at the expense that now the log-structure is not exactly
append only, and it doesn't write in exact segments. These are _partials_
originally planned for only save/restore during an unmount, but also useful
here to avoid data loss.

There's one edge case, though. Imagine there were 20 blocks instead of 14. This
would be the in layout before the 3rd write to disk:

```
In memory: [++++++++I ]
On disk:   [++++I+++XX][++++++++++]
                 ^^^    ^^^^^^^^^^ Not committed
```

Recall,

> Let's say, since there's an inode, it needs to be commited.

There's an inode, so commit, right? Well, there's no room for the map. It sure
won't be cut across 2 segments, either. The answer is to follow the same
_partials_ procedure but on an entirely empty segment, like this:

```
In memory: [        XX] (Written but left in memory!)
            ^ Segment read head for the write when its full
On disk:   [++++I+++XX][++++++++++][++++++++I ][        XX]
                                                ^ Segment write head
```

This wastes 1 block. At most, ever. If it was 21 blocks, it would waste 0. 19
would also waste 0 since there'd be 2 free blocks, and I'd just write the map no
problem. This is worst case 1 block, which is great.
