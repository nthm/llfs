#  Writing blocks to segments
## Understanding the order of writes and allocations

This is about this low level write function:
```c
// Write min(sizeof(buffer), size) data to inode data blocks at inode's offset
int inode_write_data(struct inode* inode, int offset, char* buffer, int size)
```

Note that this doesn't delete any data blocks. Ever. It can only grow a file or
overwrite data. Use `truncate` to delete data or empty the file.

This relates to transactions and commits since segments must eventually be
written to disk; it may be worth reading `writing-segments.md` to understand the
code. However, this document instead focuses on writing a buffer to a segment by
spliting it into blocks for writing, and modifying indirection blocks as needed.

It's an append only (sort of, see _"partials"_, sorry) log so any modifications
to a file must rewrite entire blocks including indirection blocks and inodes;
mostly in that order.

Here's pseudocode that explains the algorithm, roughly. Note I shorthand single
and double indirection blocks to "single" and "double".

- Check to make sure the write offset wouldn't create file holes. Don't want to
  support those in LLFS
- Setup data structures to persist during main loop such as:
  - Inode
  - Pointers to currently unallocated single and double blocks which belong to
    the inode 
  - Array of pointers to structs mapping a single belonging to the double to its
    index in the double block

The gist is that I don't write indirection blocks mixed inbetween data blocks -
I write them after. That way, the main loop can ensure only 1 block is written
to the segment each iteration; it keeps it simple.

Since the blocks aren't immediately written to the segment, they need additional
memory allocated for them. Luckily there's a max of one single, one double, and
256 singles belonging to a double, so it's 258 allocations at the very most;
they're only allocated as needed. It's 0.1MB of memory worst case so I'll just
deal with it for now.

- While(data)
  - Write block to segment
  - Calculate absolute block address
  - Want to write this to the inode so calculate indices (external call)
  - B_DIRECT?
    - Unallocate the existing block, if needed, in the inmemory FBV
    - Done. Just write to `inode->block[index] = absolute_block_address`
  - B_SINGLE?
    - Allocate, if needed, BLOCK_SIZE to the single pointer out of loop
    - If inode has a single already
      - Read (using `inode_read_data` to watchout for segments?) it in
      - Unallocate the existing block in the inmemory FBV
    - Modify the index/row to have the right block address
    - Note it's not possible to link the single to the inode yet, because it
      doesn't have a new absolute block address until its written to the segment
  - B_DOUBLE?
    - Allocate, if needed, BLOCK_SIZE to the double pointer out of loop
    - If inode has a double already
      - Read (using `inode_read_data` to watchout for segments?) it in
      - Unallocate the existing block in the inmemory FBV
    - In the pointer array, if there's no entry mapped to the `double_index`,
      allocate the mapping struct of BLOCK_SIZE + u16
    - If the single index isn't 0 (which is NULL since it's u16)
      - Read the single into the allocation
      - Unallocate the existing block in the inmemory FBV
    - Modify the index/row of the single to have the right block address
    - Note it's not possible to link the single to the double yet, because it
      doesn't have a new absolute block address until its written to the segment

There's no way for the array of structs to be written to without double being
allocated, so:

- If the double pointer isn't NULL
  - For each struct map entry
    - Write the single to the segment
    - Calculate the absolute address
    - Update the double at index/row of the map to have that absolute address
  - Write double
  - Calculate the absolute address
  - Update `inode->double_indirection_block` to point to it
- If the single pointer isn't NULL
  - Write single
  - Calculate the absolute address
  - Update `inode->single_indirection_block` to point to it
- Update the `inode->size` with some math to see if the offset + size is more
- Write inode
- Free everything (if not already does as needed)

That should be OK... :fingers_crossed:

Note that everytime I say _write x_ I also mean check if the segment is full and
needs to be written. The committing, and whether or not the inode map is needed
to be written, shrinking the segment, is transparent here. This just writes one
block at a time and doesn't need to know when the segment is writing or whether
it fit 18 or 20 blocks in it. It doesn't know if it got cleaned between block
writes, either (which is OK due to the inmemory FBV being used).

Seperation of concerns.
