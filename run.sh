#!/bin/bash
rm vdisk

# Enforce that any asserts in the code will fail the test script
set -e

make
echo
echo "Built OK"
sleep 1

./llfs_lowlevel
echo
echo "Setup disk OK"
sleep 1

./llfs_highlevel
echo
echo "Used the disk to write some content OK"
