CC=gcc
CFLAGS=-std=c11 -g
DEPS=llfs.c extern/sha1/sha1.c

all: lowlevel highlevel

lowlevel: $(DEPS)
	$(CC) $(CFLAGS) interfaces/lowlevel.c $(DEPS) -o llfs_lowlevel

highlevel: $(DEPS)
	$(CC) $(CFLAGS) interfaces/highlevel.c $(DEPS) -o llfs_highlevel

clean:
	echo "This doesn't remove your vdisk, only llfs binaries"
	rm llfs_lowlevel
	rm llfs_highlevel
