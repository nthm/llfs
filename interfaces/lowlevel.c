// Low level interface

// Using only the methods from llfs.c, this sets up a new vdisk and formats it.
// This is very basic, since inodes and data must all be written manually and
// you need to have a knowledge of what order of operations is necessary.

// See highlevel.c for a library that doesn't require you to know the nuances.
// It defines higher level operations which are easier to use.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <assert.h>

#include "../llfs.h"

void HEXDUMP (char *desc, void *addr, int len);

int main(int argc, char *argv[]) {
  char* diskname = argv[1] != NULL ? argv[1] : "vdisk";
  // Clear previous disk
  if(access(diskname, F_OK) == 0) {
    unlink(diskname);
  }

  // Setup new disk
  llfs_vdisk_create(diskname);
  llfs_vdisk_open(diskname);
  llfs_key_add("Hello");
  llfs_key_add("TestTest");
  llfs_vdisk_close();

  // Use it
  llfs_vdisk_open(diskname);
  HEXDUMP("Restored inode map", ll->imap, sizeof(ll->imap));
  llfs_set_root("Hello");
  struct inode* root_directory = (struct inode*) malloc(sizeof(struct inode));
  ll_load_inode(ll->root_inode_id, root_directory);
  HEXDUMP("Restored root directory", root_directory, sizeof(struct inode));

  // Create a new file
  int id = ll_new_inode(T_REGULAR);
  struct inode* file_inode = (struct inode*) malloc(sizeof(struct inode));
  ll_load_inode(id, file_inode);

  // Create an entry for the root directory
  struct directory_entry* dirent =
    (struct directory_entry*) malloc(sizeof(struct directory_entry));
  strcpy(dirent->filename, "FirstFile.txt");
  dirent->inode = id;
  HEXDUMP("Directory entry", dirent, sizeof(struct directory_entry));
  directory_add(root_directory, dirent);

  // Write some data to the file
  char* words = "Hello here's some content for a file";
  inode_write_data(file_inode, 0, words, strlen(words));

  // Writes the partial segment and clears all memory
  llfs_vdisk_close();
  return 0;
}
