// High level interface

// Hard to call this an "application", but an application could use this as a
// high level library to avoid interfacing with direct llfs.c calls

// This assumes a disk has already been "formatted" and setup. It just opens a
// disk and provides operations `op_*` to read and write files and directories.

// To setup a disk, run lowlevel.c

#include <stdio.h>
#include <sys/stat.h> // For `struct stat`
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include "../llfs.h"

void HEXDUMP (char *desc, void *addr, int len);

struct inode* open[MAX_INODE_COUNT];

// Open. Returns an inode ID or -1
int op_open(char* path) {
  int inode_id = resolve_path_to_inode_id(path);
  if (inode_id == -1) {
    printf("No such file or directory\n");
    return -1;
  }
  if (ll->imap[inode_id] < N_BLOCKS_RESERVED) {
    printf("Path resolves to an inode which isn't in the mapping\n");
    return -1;
  }
  // Load the inode
  struct inode* inode = (struct inode*) malloc(sizeof(struct inode));
  int status = ll_load_inode(inode_id, inode);
  if (status == -1) {
    return -1;
  }
  open[inode_id] = inode;
  return inode_id;
}

// Close. Returns 0 or -1
int op_close(int inode_id) {
  if (open[inode_id] == NULL) {
    return -1;
  }
  free(open[inode_id]);
  open[inode_id] = NULL;
  return 0;
}

// Create. Returns 0 or -1
int op_create(int parent_inode_id, char* filename) {
  // This is basically op_mkdir
  if (open[parent_inode_id] == NULL) {
    printf("Not an open inode, ID %d\n", parent_inode_id);
    return -1;
  }
  struct inode* parent_dir = open[parent_inode_id];

  int id = ll_new_inode(T_REGULAR);
  if(id == -1) {
    printf("Couldn't create the new regular inode\n");
    return -1;
  }
  struct directory_entry* entry =
    (struct directory_entry*) malloc(sizeof(struct directory_entry));
  entry->inode = id;
  strcpy(entry->filename, filename);
  // Pass up the status of 0/-1
  return directory_add(parent_dir, entry);
}

// Stat
// Information about a file. Only size for now but could have access times, etc
int op_stat(int inode_id /*, struct stat* stbuf */) {
  if (open[inode_id] == NULL) {
    printf("Not an open inode, ID %d\n", inode_id);
    return -1;
  }
  struct inode* inode = open[inode_id];
  // stbuf->st_size = inode->size;
  // stbuf->st_nlink = 1;
  if (inode->flags == T_DIRECTORY) {
    // stbuf->st_mode = S_ISDIR | 0755;
    printf("a directory ");
  } else {
    // stbuf->st_mode = S_ISREG | 0444;
    printf("a file ");
  }
  printf("of %d bytes\n", inode->size);
  return 0;
}

// Access/Existence
int op_access(char* path) {
  // Encryption, if implemented, would mean access is based on the ability to
  // walk the path at all
  int inode_id = resolve_path_to_inode_id(path);
  if (inode_id == -1) {
    return -1;
  }
  return 0;
}

// Print directory entries. Returns 0/-1
int op_readdir(int inode_id) {
  if (open[inode_id] == NULL) {
    printf("Not an open inode, ID %d\n", inode_id);
    return -1;
  }
  struct inode* inode = open[inode_id];
  if (inode->flags != T_DIRECTORY) {
    printf("Inode for ID %d isn't a T_DIRECTORY\n", inode_id);
    return -1;
  }
  int entries = inode->size / sizeof(struct directory_entry);
  if (entries == 0) {
    printf("No items\n");
    return 0;
  }
  printf("%d items\n", entries);
  
  // We want _all_ of the directory. Don't worry, it's not large to allocate.
  // An inode can only hold 32MB of data.
  // A directory is limited by the maximum number of inodes which would be a
  // u16, 65536 * 128 bytes = 8MB. However, MAX_INODE_COUNT, is the real limit
  // of inodes, limited by the inode map. 512 * 128 bytes = 0.0625MB.
  
  // TODO: I've heard it's bad practice to allocate thicc sections of the stack,
  // so this might be better off malloc'd to the heap
  struct directory_entry directory_buffer[entries];

  inode_read_data(inode, 0, (char*) directory_buffer, inode->size);

  for (int i = 0; i < entries; i++) {
    printf("%d\t%s\n", i + 1, (&directory_buffer[i])->filename);
  }
  return 0;
}

int op_mkdir(int parent_inode_id, char* directory_name) {
  if (open[parent_inode_id] == NULL) {
    printf("Not an open inode, ID %d\n", parent_inode_id);
    return -1;
  }
  struct inode* parent_dir = open[parent_inode_id];

  int id = ll_new_inode(T_DIRECTORY);
  if(id == -1) {
    printf("Couldn't create the new directory inode\n");
    return -1;
  }
  struct directory_entry* entry =
    (struct directory_entry*) malloc(sizeof(struct directory_entry));
  entry->inode = id;
  strcpy(entry->filename, directory_name);
  directory_add(parent_dir, entry);
  return 0;
}

// Truncate
int op_truncate(char* path, int new_size) {
  // Resolve to inode
  // Similar to inode_read_data, walk the block tree to inode->size
  // Skip all blocks less than new_size
  // Handle the partial read/write if new_size % BLOCK_SIZE != 0
  // Unallocate the blocks
  // If relative inode block is > 256, unallocate the single
  // As inode block becomes > multiples of 256, unallocate double singles
  // Unallocate the double if it really gets to that
  // Set a new inode size and null out all block pointers. Write it
}

// Read `size` data of inode from `offset`. Returns the bytes read or -1
int op_read(int inode_id, int offset, char* buffer, int size) {
  if (open[inode_id] == NULL) {
    printf("Not an open inode, ID %d\n", inode_id);
    return -1;
  }
  struct inode* inode = open[inode_id];
  return inode_read_data(inode, offset, buffer, size);
}

// Write `size` data of inode from `offset`. Returns the bytes written or -1
int op_write(int inode_id, int offset, char* buffer, int size) {
  if (open[inode_id] == NULL) {
    printf("Not an open inode, ID %d\n", inode_id);
    return -1;
  }
  struct inode* inode = open[inode_id];
  return inode_write_data(inode, offset, buffer, size);
}

int main(int argc, char *argv[]) {
  assert(llfs_vdisk_open("vdisk") == 0);
  HEXDUMP("100 bytes of restored inode map", ll->imap, 100);

  // Root A
  // These keys, Hello, and TestTest are set in lowlevel.c
  assert(llfs_set_root("Hello") == 0);
  int root_id = op_open("/");
  assert(root_id != -1);
  
  // List directory (prints to stdout)
  printf(">>> LISTING DIRECTORY\n");
  assert(op_readdir(root_id) == 0);

  // Create file
  printf(">>> CREATING FILE\n");
  assert(op_create(root_id, "content.txt") == 0);
  // Ensure duplicate files cannot be created, using -1 for failure
  assert(op_create(root_id, "content.txt") == -1);

  char path_0[] = "/content.txt";
  int file_id = op_open(path_0);
  assert(file_id != -1);
  char* content = "This is some content in LLFS";
  printf(">>> WRITING CONTENT\n");
  assert(op_write(file_id, 0, content, strlen(content)) == strlen(content));

  // Save to disk, for debugging in hexdump
  ll_write_imap_to_segment_end();
  ll_flush_segment();
  ll_update_checkpoint_block_offset();

  // Read part of the data back into a buffer
  char content_read_buffer[30];
  printf(">>> READING CONTENT\n");
  assert(op_read(file_id, 5, content_read_buffer, 15) == 15);
  HEXDUMP(">>> Content read from disk", content_read_buffer, 15);
  assert(strncmp(content_read_buffer, "is some content", 15) == 0);

  // Save to disk, for debugging in hexdump
  // ll_write_imap_to_segment_end();
  // ll_flush_segment();
  // ll_update_checkpoint_block_offset();

  printf(">>> CREATING SUB DIRECTORY\n");
  assert(op_mkdir(root_id, "subdir") == 0);
  char path_1[] = "/subdir";
  int subdir_id = op_open(path_1);
  assert(subdir_id != -1);

  // List directory (prints to stdout)
  printf(">>> LISTING DIRECTORY\n");
  assert(op_readdir(root_id) == 0);

  printf(">>> CREATING CONTENT IN SUB DIRECTORY\n");
  assert(op_create(subdir_id, "content-within-subdir.txt") == 0);

  // List directory (prints to stdout)
  printf(">>> LISTING DIRECTORY\n");
  assert(op_readdir(subdir_id) == 0);

  // Internally op_open uses strtok, which segfaults if the string is constant
  char path_2[] = "/subdir/content-within-subdir.txt";
  // Just reuse the file_id
  file_id = op_open(path_2);
  assert(file_id != -1);
  printf(">>> WRITING CONTENT IN SUB DIRECTORY FILE\n");
  assert(op_write(file_id, 0, "Hi", 2) == 2);

  // Root B
  printf(">>> \n");
  printf(">>> \n");
  printf(">>> SWITCHING ROOT\n");
  llfs_set_root("TestTest");
  // Just reuse the root_id
  root_id = op_open("/");
  printf(">>> LISTING DIRECTORY\n");
  assert(op_readdir(root_id) == 0);

  char dest[15];
  for (int i = 0; i < 4; i++) {
    sprintf(dest, "File_%d.txt", i);

    printf(">>> CREATING FILE '%s'\n", dest);
    assert(op_create(root_id, dest) == 0);

    printf(">>> LISTING DIRECTORY\n");
    assert(op_readdir(root_id) == 0);
  }
}
