#pragma once

#ifndef LLFS
#define LLFS

#include <stdint.h> // For uint*_t
#include "./extern/sha1/libsha1.h"

// Create symbol for linking
extern int min(int a, int b);

#define LLFS_MAGIC "\xDE\xAD\xDE\xAD"

#define BLOCK_SIZE 512
#define VDISK_SIZE 2 * 1024 * 1024 // 2MB

#define N_BLOCKS_PER_SEGMENT 20
#define BYTES_PER_SEGMENT BLOCK_SIZE * N_BLOCKS_PER_SEGMENT

#define N_BLOCKS VDISK_SIZE / BLOCK_SIZE
#define N_BLOCKS_RESERVED 10

// From the spec, which suggests data blocks start here
#define DATA_OFFSET BLOCK_SIZE * N_BLOCKS_RESERVED

// How many blocks are needed to address all data blocks 
// Faking this value for now since I'd need to write code to overallocate when
// the number, such as 4086 in the 2MB case, isn't divisible nicely
#define N_BLOCKS_FBV 1
// #define BLOCK_SIZE_BITS BLOCK_SIZE * 8
// #define N_BLOCKS_FBV N_BLOCKS_DATA / BLOCK_SIZE_BITS

// Number of used segments before the cleaner is called
#define CLEAN_THRESHOLD 5

// This is based on the inode map, which doesn't scale, and is made to _always_
// take up the last 2 blocks of a segment. 2 blocks is 1024 bytes, and 2 bytes
// are needed for block numbers. The offset is the inode ID.
#define MAX_INODE_COUNT 512

extern int INODE_MAP_BLOCK_COUNT;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

enum inode_types { T_REGULAR, T_DIRECTORY, T_SYMLINK };

// On disk structs:

struct superblock {
  char magic[4];
  u16  block_limit;
  u16  inode_limit;
  u16  bytes_per_block;
  u16  inode_map_checkpoint;
  // Unfortunately, for now, IDs for inodes are just auto-incrementing
  // This needs to be replaced, see readme#TODO
  u16  saved_inode_count;
  // These `saved_*` fields are for restoring partial segment writes
  u16  saved_memory_segment_block;
  u16  saved_disk_segment_wh;
  // Uses u16 to be consistent but the superblock can only hold 20 entries
  // Change this if the block size changes, or if they're allocated elsewhere
  u16  key_slots;
} __attribute__((packed));

// 22 bytes each
// This is stored _inside_ the superblock's block, but after the above struct
// Superblock fits (512 - 18) / 22 = ~20 entries
struct key_slot {
  char sha1[SHA1_DIGEST_SIZE];
  u16  inode_id;
} __attribute__((packed));

// To keep it simple it's 1 inode per block, so max size is BLOCK_SIZE
struct inode {
  u16  id;
  // Single indirects hold 512 / sizeof(u16) = 256 blocks. Doubles hold 256^2
  // With direct blocks that's 65802 blocks or 32.12MB per file; needs u32
  u32  size;
  // TODO: There's no reason for this to be u32 beyond the spec
  // Could store a bitmap similar to the FBV for multiple future flags
  u32  flags;
  u16 blocks[10];
  u16 single_indirection_block;
  u16 double_indirection_block;
  // IV should match the AES block size of 16 bytes
  char iv[16];
  // TODO: Metadata struct to fill the remaining
} __attribute__((packed));

struct indirection_block {
  u16 next[BLOCK_SIZE / sizeof(u16)];
} __attribute__((packed));

// 128 bytes each so 4 entries per block
struct directory_entry {
  u16  inode;
  char filename[126];
} __attribute__((packed));

// In memory structs:

struct llfs {
  // Pointer to the mounted/open vdisk
  FILE*  vdisk;
  // Password (Allows multitenancy but encryption is not yet implemented)
  char*  key;
  // Root directory for the key. Looked up by hash under the superblock
  u16    root_inode_id;
  // Free block vector, block 1, which is periodically written back
  // TODO: The FBV only need to track N_BLOCKS_DATA, but that's a lot of code
  u32    block_bitmap[(N_BLOCKS_FBV * BLOCK_SIZE * 8) / (sizeof(32) * 8)];
  // The segment that will be written to disk once full
  char*  memory_segment;
  // Where data blocks will be written in `memory segment` (block precision)
  u16    memory_segment_block;
  // Where `memory_segment` will be written to disk (segment precision)
  u16    disk_segment_wh;
  // Boolean, if the inode map need to be written this segment write
  int    commit_required;
  // Inode map
  u16    imap[MAX_INODE_COUNT];
  // Block where the inode map was last written
  u16    inode_map_checkpoint_block;
  // Current number of inodes. Incremented for IDs when they're created
  u16    inode_count;
};

struct indirection_index_map {
  struct indirection_block* single;
  int    index;
};

// Global runtime state object
struct llfs* ll;

int llfs_vdisk_create(char* filepath);
int llfs_vdisk_open(char* filepath);
int llfs_vdisk_close();
int llfs_key_add(char* key);
int llfs_set_root(char* key);

// Return a status code of 0/-1:

int ll_read_disk(int abs_block, int offset, void* buffer, int size);
int ll_read(int abs_block, int offset, void* buffer, int size);
int ll_load_inode(int inode_id, struct inode* inode_buffer);

// Return value or -1:

int ll_new_inode(enum inode_types type);
int inode_read_data(struct inode* inode, int offset, char* buffer, int size);
int inode_write_data(struct inode* inode, int offset, char* buffer, int size);
int resolve_path_to_inode_id(char* path);
int directory_name_to_inode_id(struct inode* directory, char* name);
int directory_add(struct inode* directory, struct directory_entry* entry);

// Return void. They're safe and don't perform allocations, file read/writes, or
// accept any parameters which need validation beyond type checking:

void ll_write_block(void* buffer);
void ll_write_inode(struct inode* inode);
void ll_write_imap_to_segment_end();
void ll_update_checkpoint_block_offset();
void ll_flush_segment();
void ll_load_next_segment();

#endif // ifndef LLFS