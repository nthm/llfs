#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h> // For offsetof()
#include <unistd.h>

#include "./extern/echoless-getchar.c"
#include "./extern/bitmap.c"
#include "./extern/sha1/libsha1.h"
#include "./extern/debug.c"
#include "llfs.h"

inline int min(int a, int b) {
  if (a > b) return b;
  return a;
}

// These weren't working in the llfs.h due to the order of operations
// I understand why #define macros aren't recommended for math...
int N_BLOCKS_DATA = N_BLOCKS - N_BLOCKS_RESERVED;

// Likewise, using N_BLOCKS_DATA instead of 4086 isn't appreciated by gcc
int N_SEGMENTS = 4086 / N_BLOCKS_PER_SEGMENT;
int INODE_MAP_BLOCK_COUNT = (sizeof(uint16_t) * MAX_INODE_COUNT) / BLOCK_SIZE;

// Disk setup, key management, and mounting
// -----------------------------------------------------------------------------

// Initiate an empty disk. Superblock and FBV only. No keys. Returns 0 or -1
int llfs_vdisk_create(char* filepath) {
  PRINT("Creating a %d byte vdisk", VDISK_SIZE);
  FILE* file= fopen(filepath, "wb");
  if (file == NULL) {
    return -1;
  }
  struct superblock* superblock =
    (struct superblock*) malloc(sizeof(struct superblock));
  memcpy(superblock->magic, LLFS_MAGIC, 4);
  superblock->block_limit = N_BLOCKS;
  superblock->inode_limit = MAX_INODE_COUNT;
  superblock->bytes_per_block = BLOCK_SIZE;
  superblock->inode_map_checkpoint = 0;
  superblock->saved_inode_count = 0;
  superblock->saved_disk_segment_wh = 0;
  superblock->saved_memory_segment_block = 0;
  superblock->key_slots = 0;
  PRINT("Superblock size: %d bytes", sizeof(struct superblock));
  fseek(file, 0, SEEK_SET);
  fwrite(superblock, sizeof(struct superblock), 1, file);
  free(superblock);

  // Build the free_block_vector
  // TODO: Note that N_BLOCKS_FBV is skewed in File.h
  // TODO: This undercuts any numbers that aren't divisible by 32
  int bitmap_bits_needed = sizeof(ll->block_bitmap) * 8;
  PRINT("Bitmap size: %d bits", bitmap_bits_needed);
  u32 bitmap[bitmap_bits_needed / (sizeof(u32) * 8)];

  // TODO: Remove this. Was only for testing:
  for (int i = 0; i < bitmap_bits_needed; i++) {
    if (i % 1024 == 0) {
      set_bit(bitmap, i);
    } else {
      clear_bit(bitmap, i);
    }
  }

  PRINT("Writing FBV bitmap at %d bytes", BLOCK_SIZE);
  fseek(file, BLOCK_SIZE, SEEK_SET);
  fwrite(bitmap, sizeof(bitmap), 1, file);

  fseek(file, VDISK_SIZE - 1, SEEK_SET);
  fwrite("", sizeof(char), 1, file);

  fclose(file);
  return 0;
}

// Open the disk. Returns 0 or -1
int llfs_vdisk_open(char* filepath) {
  if(access(filepath, F_OK) == -1) {
    // Disk doesn't exist at that path
    PRINT("No disk at filepath '%s'", filepath);
    return -1;
  }
  FILE* file= fopen(filepath, "r+");
  if (file == NULL) {
    PRINT("Couldn't open disk");
    return -1;
  }
  struct superblock* superblock =
    (struct superblock*) malloc(sizeof(struct superblock));
  fseek(file, 0, SEEK_SET);
  int read = fread(superblock, 1, sizeof(struct superblock), file);
  PRINT("Read a %d byte superblock", read);
  if (strncmp(superblock->magic, LLFS_MAGIC, 4) != 0) {
    PRINT("Not an LLFS superblock, refusing");
    return -1;
  }
  if (superblock->bytes_per_block != BLOCK_SIZE) {
    PRINT("TODO: Support block size of %d...", superblock->bytes_per_block);
    return -1;
  }

  // Could do other FS checks here, but assume it's OK and continue
  ll = (struct llfs*) malloc(sizeof(struct llfs));
  ll->vdisk = file;

  // Load key slot. Note that `0` is intentionally invalid
  ll->key = NULL;
  ll->root_inode_id = 0;

  // Initialize the segment and restore partial data as needed
  ll->memory_segment = (char*) malloc(BYTES_PER_SEGMENT);

  ll->disk_segment_wh = superblock->saved_disk_segment_wh;
  int memory_segment_block = superblock->saved_memory_segment_block;
  ll->memory_segment_block = memory_segment_block;
  if (memory_segment_block != 0) {
    // Restore
    PRINT("Restoring the partial segment by loading into memory");
    int start = DATA_OFFSET + (ll->disk_segment_wh * BYTES_PER_SEGMENT);
    fseek(ll->vdisk, start, SEEK_SET);
    fread(ll->memory_segment, BLOCK_SIZE, memory_segment_block, ll->vdisk);
    HEXDUMP("Restored segment", ll->memory_segment, BYTES_PER_SEGMENT);
  }

  // From block 1, for 1 block, read the bitmap
  fseek(ll->vdisk, BLOCK_SIZE * 1, SEEK_SET);
  fread(ll->block_bitmap, BLOCK_SIZE, N_BLOCKS_FBV, ll->vdisk);

  // Restore the last known inode map from the log
  ll->inode_map_checkpoint_block = superblock->inode_map_checkpoint;
  if (superblock->inode_map_checkpoint > 0) {
    PRINT("Restoring inode map from block %d", ll->inode_map_checkpoint_block);
    fseek(ll->vdisk, superblock->inode_map_checkpoint * BLOCK_SIZE, SEEK_SET);
    fread(ll->imap, sizeof(u16), MAX_INODE_COUNT, ll->vdisk);
  } else {
    memset(ll->imap, 0, sizeof(ll->imap));
    // Allow it to be distinguishable in the hex print
    // memset(ll->imap, 0xFF, 100);
  }

  // Restore the inode counter so new inodes don't overwrite map entries
  ll->inode_count = superblock->saved_inode_count;
  PRINT("Restoring inode counter; new inodes start at ID %d", ll->inode_count);
  return 0;
}

// Add key to the superblock. Returns 0 or -1
int llfs_key_add(char* key) {
  PRINT("Adding key: %s", key);
  if (ll == NULL) {
    PRINT("No ll-> initiated. Open a vdisk first");
    return -1;
  }
  sha1_ctx ctx;
  char hash[SHA1_DIGEST_SIZE];
  sha1_begin(&ctx);
  sha1_hash(key, strlen(key), &ctx);
  sha1_end(hash, &ctx);
  HEXDUMP("Hashed key", &hash, SHA1_DIGEST_SIZE);

  struct superblock* superblock = (struct superblock*) malloc(BLOCK_SIZE);
  fseek(ll->vdisk, 0, SEEK_SET);
  fread(superblock, BLOCK_SIZE, 1, ll->vdisk);
  u16 key_slot_count = superblock->key_slots;
  int key_slot_size = sizeof(struct key_slot);
  int max_key_slots = (BLOCK_SIZE - sizeof(struct superblock)) / key_slot_size;
  PRINT("%d used key slots. %d total", key_slot_count, max_key_slots);
  int available_key_slots = max_key_slots - key_slot_count;
  if (available_key_slots < 0) {
    PRINT("BAD ASSERT: Math error for key slots? Available < 0");
    return -1;
  }
  if (available_key_slots == 0) {
    PRINT("No key slots available in this vdisk");
    return -1;
  }
  struct key_slot* key_slot = (struct key_slot*) malloc(key_slot_size);
  memcpy(key_slot->sha1, hash, SHA1_DIGEST_SIZE);
  int inode_id = ll_new_inode(T_DIRECTORY);
  key_slot->inode_id = inode_id;

  PRINT("Writing key slot (hash + root inode)");
  int new_key_wh = sizeof(struct superblock) + (key_slot_size * key_slot_count);
  PRINT("Offset: 0x%04x", new_key_wh);
  fseek(ll->vdisk, new_key_wh, SEEK_SET);
  // Note that the segment hasn't been flushed, so there's a chance it's lost
  // and this is pointing to nothing...
  int wrote;
  wrote = fwrite(key_slot, 1, key_slot_size, ll->vdisk);
  PRINT("Wrote %d bytes", wrote);
  free(key_slot);

  // Update the superblock
  key_slot_count++;
  PRINT("New key slot count %d", key_slot_count);
  fseek(ll->vdisk, offsetof(struct superblock, key_slots), SEEK_SET);
  wrote = fwrite(&key_slot_count, 1, sizeof(u16), ll->vdisk);
  PRINT("Wrote %d bytes at 0x%04x", wrote, offsetof(struct superblock, key_slots));
  return 0;
}

// Pivot to the root corresponding to the given unhashed key. Returns 0 or -1
int llfs_set_root(char* key) {
  PRINT("Searching for key: %s", key);
  if (ll == NULL) {
    PRINT("No ll-> initiated. Open a vdisk first");
    return -1;
  }
  // Hash the key
  sha1_ctx ctx;
  char hash[SHA1_DIGEST_SIZE];
  sha1_begin(&ctx);
  sha1_hash(key, strlen(key), &ctx);
  sha1_end(hash, &ctx);
  HEXDUMP("Hashed key", &hash, SHA1_DIGEST_SIZE);

  u16 key_slots;
  fseek(ll->vdisk, offsetof(struct superblock, key_slots), SEEK_SET);
  fread(&key_slots, sizeof(u16), 1, ll->vdisk);

  struct key_slot keys_buffer[key_slots];
  fseek(ll->vdisk, sizeof(struct superblock), SEEK_SET);
  int read = fread(&keys_buffer, sizeof(struct key_slot), key_slots, ll->vdisk);
  PRINT("Read %d keys into buffer at %p", read, keys_buffer);

  // TODO: Factor this out and use it in key_add() to avoid duplicate keys
  // Find the key_slot for the given key
  struct key_slot* matched_key_slot = NULL;
  for (int i = 0; i < key_slots; i++) {
    struct key_slot key_slot = keys_buffer[i];
    char desc[15];
    sprintf(desc, "Key slot #%d", i);
    HEXDUMP(desc, &key_slot, sizeof(key_slot));
    if (memcmp((&key_slot)->sha1, hash, SHA1_DIGEST_SIZE) == 0) {
      // Match
      matched_key_slot = &key_slot;
      PRINT("Key found");
      break;
    }
  }
  if (matched_key_slot == NULL) {
    // No match
    PRINT("Key not found. You can add it with llfs_key_add()");
    return -1;
  }
  PRINT("Matched key has inode ID %d", matched_key_slot->inode_id);
  if (ll->imap[matched_key_slot->inode_id] < N_BLOCKS_RESERVED) {
    PRINT("BAD ASSERT: Root inode for key is below DATA_OFFSET");
    exit(EXIT_FAILURE);
  }
  // Load key slot
  ll->key = key;
  ll->root_inode_id = matched_key_slot->inode_id;
  return 0;
}

// Close the opened vdisk and write any partial segment data. Returns 0 or -1
int llfs_vdisk_close() {
  if (ll == NULL) {
    PRINT("No ll-> initiated. Open a vdisk first");
    return -1;
  }
  // This is always safe to do because write_block and write_inode would handle
  // cases where the map wouldn't have space to be written
  ll_write_imap_to_segment_end();
  ll_flush_segment();
  ll_update_checkpoint_block_offset();
  fflush(ll->vdisk);
  fclose(ll->vdisk);
  free(ll->memory_segment);
  free(ll);
  return 0;
}

// Read and write operations
// -----------------------------------------------------------------------------

inline u16 absolute_memory_segment_block() {
  return (
    N_BLOCKS_RESERVED +
    ll->disk_segment_wh * N_BLOCKS_PER_SEGMENT +
    ll->memory_segment_block
  );
}
// Create a symbol for linking
u16 absolute_memory_segment_block();

// Read an absolute block from disk. Returns 0 or -1
int ll_read_disk(int abs_block, int offset, void* buffer, int size) {
  fseek(ll->vdisk, abs_block * BLOCK_SIZE + offset, SEEK_SET);
  int read = fread(buffer, 1, size, ll->vdisk);
  if (read != size) {
    return -1;
  }
  return 0;
}

// Read an absolute block either from the segment or disk. Returns 0 or -1
int ll_read(int abs_block, int offset, void* buffer, int size) {
  if (offset < 0) {
    PRINT("Refusing. Asked for a negative offset");
    return -1;
  }
  // TODO: An alternative would be to drop the `size` parameter and use offset
  // to calculate it in here
  if (offset + size > BLOCK_SIZE) {
    // Can't be sure that the neighbouring blocks are also either on disk or in
    // segment, so it's only safe to read one
    PRINT("Refusing. Asked to read across blocks");
    return -1;
  }
  if (abs_block < N_BLOCKS_RESERVED) {
    PRINT("Trying to read block %d; outside of the log", abs_block);
    return -1;
  }

  // Block is in memory when it's within those of the next segment to be written
  int segment_start_block =
    N_BLOCKS_RESERVED + ll->disk_segment_wh * N_BLOCKS_PER_SEGMENT;
  int segment_end_block = segment_start_block + N_BLOCKS_PER_SEGMENT - 1;
  
  if (
    abs_block >= segment_start_block &&
    abs_block <= segment_end_block
  ) {
    // Block is in memory
    PRINT("Block %d is in memory", abs_block);
    int memory_block_byte_offset =
      (abs_block - segment_start_block) * BLOCK_SIZE + offset;
    PRINT("Translated to segment block %d", (abs_block - segment_start_block));
    PRINT("Segment read byte offset %d bytes", memory_block_byte_offset);
    memcpy(buffer, ll->memory_segment + memory_block_byte_offset, size);
  } else {
    // Block is on disk
    PRINT("Block %d is on disk", abs_block);
    if (get_bit(ll->block_bitmap, abs_block) == 1) {
      // TODO: I'd assert this if the FBV was working. Haven't gotten to it yet
      PRINT("BAD: Inode points to block %d marked 1 in FBV", abs_block);
    }
    int read = ll_read_disk(abs_block, offset, buffer, size);
    if (read == -1) {
      PRINT("Issues reading from ll_read_disk() during ll_read()");
      return -1;
    }
  }
  return 0;
}

// Load an inode of a given ID into the given buffer. Returns 0 or -1
int ll_load_inode(int inode_id, struct inode* inode_buffer) {
  int absolute_block = ll->imap[inode_id];
  // TODO: The 0xFF is mostly for debugging. Should pick zero or something...
  if (absolute_block == 0xFF) {
    PRINT("No such inode %d", inode_id);
    return -1;
  }
  if (absolute_block < N_BLOCKS_RESERVED) {
    PRINT("BAD ASSERT: Inode block is reserved: block %d", absolute_block);
    exit(EXIT_FAILURE);
  }
  // Pass the status (0/-1) up
  return ll_read(absolute_block, 0, inode_buffer, sizeof(struct inode));
}

// Create an inode and add it to the segment. Returns inode ID or -1
int ll_new_inode(enum inode_types type) {
  struct inode* ino = (struct inode*) malloc(sizeof(struct inode));
  if (ll->inode_count > MAX_INODE_COUNT) {
    // TODO: Can't use inode_count as an ID field since removing doesn't free
    // them. Will need to have a method/loop that reads the map to find the next
    // available ID to recycle
    PRINT("No more inodes");
    return -1;
  }
  // TODO: BAD. See readme for the issues with this
  int id = ll->inode_count++;
  ino->id = id;
  ino->flags = type;
  ino->size = 0;
  // Must be added since an inode must be in the segment or disk to be acessed
  // by ll_read() in order to later have data associated with it
  ll_write_inode(ino);
  free(ino);
  return id;
}

// Writes a general data block. Void return since it should always work
void ll_write_block(void* buffer) {
  // TODO: If a block's being written, it doesn't have an inode. That's how this
  // works with the ordering of an LFS. Add it to the uncommited FBV

  // The block is an integer, a block, so move that many bytes ahead
  void* segment_wh = ll->memory_segment + ll->memory_segment_block * BLOCK_SIZE;
  PRINT("Writing block to segment block %d (offset %d bytes)",
    ll->memory_segment_block, ll->memory_segment_block * BLOCK_SIZE);
  memcpy(segment_wh, buffer, BLOCK_SIZE);
  ll->memory_segment_block++;

  int limit;
  // Is there an inode or are we overshot and need to write right now
  if (ll->commit_required == 1) {
    limit = N_BLOCKS_PER_SEGMENT - INODE_MAP_BLOCK_COUNT;
    if (ll->memory_segment_block == limit) {
      // The overshoot case, when the inode map doesn't fit, is handled by
      // ll_write_inode()
      PRINT("Flushing segment");
      PRINT("Commit required, so adding the inode map to the segment");
      ll_write_imap_to_segment_end();
      ll_flush_segment();
      ll_load_next_segment();
      ll->commit_required = 0;
    }
  } else {
    limit = N_BLOCKS_PER_SEGMENT;
    if (ll->memory_segment_block == limit) {
      PRINT("Flushing segment");
      ll_flush_segment();
      ll_load_next_segment();
    }
  }
}

// Write an inode, enforcing a commit. Void return since it should always work
void ll_write_inode(struct inode* inode) {
  // Save the memory_segment_block before ll_write_block updates it
  ll->imap[inode->id] = absolute_memory_segment_block();
  PRINT("Mapped ID %d to absolute block %d", inode->id, ll->imap[inode->id]);
  ll->commit_required = 1;

  char* block = (char*) malloc(BLOCK_SIZE);
  memset(block, 0, BLOCK_SIZE);
  // Make the inode distinguishable in the hex print
  // memset(block, 0x77, 64);
  memcpy(block, inode, sizeof(struct inode));
  HEXDUMP("Inode", block, sizeof(struct inode));

  // This flushes the segment, and if it can, it'll write the inode map
  ll_write_block(block);

  // FIXME: Also see the other FIXME in this file, throws saying it's invalid
  // pointer even. Same address: 0x555555667a0 at malloc and at free. This works
  // for several inodes. Then one day it just doesn't. Needs review
  // free(block);

  // However, there's an edge case: If a data block is written just before the
  // spot for the inode map, and there was no inode in the segment yet,
  // ll_write_block is allowed to use all the space. Writing one in now means a
  // map needs to be commited, but there's no space. It decides to do nothing,
  // so handle that here:

  int limit = N_BLOCKS_PER_SEGMENT - INODE_MAP_BLOCK_COUNT;
  if (ll->memory_segment_block > limit) {
    PRINT("Flushing segment since we're over limit. Need a partial write...");
    ll_flush_segment();
    ll_load_next_segment();

    // Write the map into the new, empty segment
    ll_write_imap_to_segment_end();
    ll_flush_segment();
    // This works because ll_load_next_segment wasn't called to update disk wh
    ll_update_checkpoint_block_offset();
  }
}

void ll_write_imap_to_segment_end() {
  if (N_BLOCKS_PER_SEGMENT - ll->memory_segment_block < INODE_MAP_BLOCK_COUNT) {
    PRINT("BAD ASSERT: There's no room to write the inode map to the segment");
  }
  int byte_offset = BYTES_PER_SEGMENT - (INODE_MAP_BLOCK_COUNT * BLOCK_SIZE);
  PRINT("Inode map byte offset %d", byte_offset);
  HEXDUMP("100 bytes of the inode map", ll->imap, 100);
  void* wh = ll->memory_segment + byte_offset;
  memcpy(wh, ll->imap, sizeof(ll->imap));
}

// Update the superblock to point to the new inodemap location
// This doesn't write the map. That happens within the segment write
void ll_update_checkpoint_block_offset() {
  int absolute_segment_start_block =
    N_BLOCKS_RESERVED + (ll->disk_segment_wh * N_BLOCKS_PER_SEGMENT);
  int inode_map_block =
    absolute_segment_start_block + N_BLOCKS_PER_SEGMENT - INODE_MAP_BLOCK_COUNT;
  PRINT("Inode map is at block %d", inode_map_block);

  // Save fields to the superblock
  int wh;
  wh = offsetof(struct superblock, inode_map_checkpoint);
  fseek(ll->vdisk, wh, SEEK_SET);
  fwrite(&inode_map_block, sizeof(u16), 1, ll->vdisk);

  wh = offsetof(struct superblock, saved_inode_count);
  fseek(ll->vdisk, wh, SEEK_SET);
  fwrite(&ll->inode_count, sizeof(u16), 1, ll->vdisk);

  // Mark the old map as free
  for (int i = 0; i < INODE_MAP_BLOCK_COUNT; i++) {
    if (ll->inode_map_checkpoint_block > N_BLOCKS_RESERVED) {
      set_bit(ll->block_bitmap, ll->inode_map_checkpoint_block + i);
    }
    clear_bit(ll->block_bitmap, inode_map_block + i);
  }
  // Write the FBV
  // TODO: Still need to support 2 different in-memory FBVs...
  fseek(ll->vdisk, BLOCK_SIZE, SEEK_SET);
  fwrite(ll->block_bitmap, sizeof(ll->block_bitmap), 1, ll->vdisk);
  // Update memory
  ll->inode_map_checkpoint_block = inode_map_block;
}

void ll_flush_segment() {
  int offset = DATA_OFFSET + ll->disk_segment_wh * BYTES_PER_SEGMENT;
  PRINT("Flushing segment to offset %d bytes", offset);
  fseek(ll->vdisk, offset, SEEK_SET);
  HEXDUMP("Segment", ll->memory_segment, BYTES_PER_SEGMENT);
  size_t wrote = fwrite(ll->memory_segment, 1, BYTES_PER_SEGMENT, ll->vdisk);
  if (wrote != BYTES_PER_SEGMENT) {
    PRINT("BAD ASSERT: Wasn't able to write the full segment buffer");
    exit(EXIT_FAILURE);
  }
  // Update the superblock saved_* entries for restoration. Except the inode
  // count, since that is only updated on a commit during an inode map write
  int wh;
  wh = offsetof(struct superblock, saved_memory_segment_block);
  fseek(ll->vdisk, wh, SEEK_SET);
  fwrite(&ll->memory_segment_block, sizeof(u16), 1, ll->vdisk);

  wh = offsetof(struct superblock, saved_disk_segment_wh);
  fseek(ll->vdisk, wh, SEEK_SET);
  fwrite(&ll->disk_segment_wh, sizeof(u16), 1, ll->vdisk);
}

void ll_load_next_segment() {
  PRINT("Clearing segment and loading the next available one");
  // Clear memory_segment
  memset((void*) ll->memory_segment, 0, BYTES_PER_SEGMENT);
  ll->memory_segment_block = 0;

  // For now, since there's not much time in this assignment, just move to the
  // next location. This won't work beyond the 2MB
  ll->disk_segment_wh++;
  return;

  // The rest of this code is unreachable. It's the real algorithm that would
  // handle choosing the correct next available segment, and do cleaning. To be
  // useful though it needs the FBV to be working. See TODOs

  // Determine the free blocks of each segment
  int next_avail_segment = -1;
  int segment_number, avail_segment_count;
  // For debugging
  // N_BLOCKS_PER_SEGMENT means the segment is available
  int segment_avail_block_counts[N_SEGMENTS];

  // Need to run through all data blocks anyway to know if the disk needs to be
  // cleaned, so finding the next available segment doesn't shortcut early
  for (int i = 0; i < N_BLOCKS_DATA; i++) {
    // Start looping at the next segment, since we want the next available one
    // to be physically _after_ the current, when possible
    i = (i + (ll->disk_segment_wh * N_BLOCKS_PER_SEGMENT)) % N_BLOCKS_DATA;
    segment_number = i / N_BLOCKS_PER_SEGMENT;
    if (
      get_bit(ll->block_bitmap, i) == 1 &&
      ++segment_avail_block_counts[segment_number] == N_BLOCKS_PER_SEGMENT
    ) {
      avail_segment_count++;
      if (next_avail_segment == -1) {
        next_avail_segment = segment_number;
      }
    }
  }
  PRINT("Segment available block counts:");
  for (int i = 0; i < N_SEGMENTS; i++) {
    PRINT("Segment %d\tAvailable blocks: %d", i, segment_avail_block_counts[i]);
  }

  if (next_avail_segment == -1) {
    PRINT("BAD ASSERT: There are no available segments to write to");
    exit(EXIT_FAILURE);
  }

  // Point to the next available segment, which was calculated above...
  ll->disk_segment_wh = next_avail_segment;

  if(avail_segment_count <= CLEAN_THRESHOLD) {
    PRINT("Would clean FS: %d <= %d", avail_segment_count, CLEAN_THRESHOLD);
    // llfs_clean();
  }
  return;
}

// Inode operations and path resolution
// -----------------------------------------------------------------------------

// Helps convert a block to an absolute block, but only does the math instead of
// the actual lookup since that would require block memory allocation which
// woube done by the caller since it is likely repeatative
enum index_block { B_DIRECT, B_SINGLE, B_DOUBLE };
enum index_block inode_block_to_indirection_indices(
  u16 inode_block, u8* direct_index, u8* single_index, u8* double_index) {

  // Setting to 0 is a valid index, but think of it as NULL; they're not used

  // Direct blocks
  if (inode_block < 10) {
    *direct_index = (u8) inode_block;
    *single_index = 0;
    *double_index = 0;
    return B_DIRECT;
  }
  // Single indirect
  inode_block -= 10;
  if (inode_block < 256) {
    *direct_index = 0;
    *single_index = (u8) inode_block;
    *double_index = 0;
    return B_SINGLE;
  }
  // Double indirect
  inode_block -= 256;
  if (inode_block < (10 + 256 + (256 * 256))) {
    // Assuming a double indirect block holds 256 u16 entries
    *direct_index = 0;
    *double_index = (u8) (inode_block / 256);
    *single_index = (u8) (inode_block % 256);
    return B_DOUBLE;
  }
  // This should never happen
  PRINT("BAD ASSERT: Inode block %d is beyond highest block", inode_block);
  exit(EXIT_FAILURE);
}

// Read `size` data of inode from `offset`. Returns the bytes read or -1
int inode_read_data(struct inode* inode, int offset, char* buffer, int size) {
  // If they ask for an offset that's entirely out of range
  if (offset >= inode->size) {
    PRINT("Asked for offset %d bytes but file size only %d bytes",
      offset, inode->size);
    return -1;
  }
  int inode_block = offset / BLOCK_SIZE;
  int inode_subblock_offset = offset % BLOCK_SIZE;
  // Number of bytes read
  int read = 0;
  // Relative blocks are in the inode. Absolute is the entire disk
  u16 absolute_block;
  u8 direct_index, single_index, double_index, double_index_shortcut;
  struct indirection_block* single_block = NULL;
  struct indirection_block* double_block = NULL;
  while (read < size && read < inode->size) {
    enum index_block lookup_type =
      inode_block_to_indirection_indices(
        inode_block, &direct_index, &single_index, &double_index);
    
    switch(lookup_type) {
      case B_DIRECT:
        absolute_block = inode->blocks[direct_index];
        break;
      case B_SINGLE:
        if (single_block == NULL) {
          single_block = (struct indirection_block*) malloc(BLOCK_SIZE);
          ll_read(inode->single_indirection_block, 0, single_block, BLOCK_SIZE);
        }
        absolute_block = single_block->next[single_index];
        break;
      case B_DOUBLE:
        if (double_block == NULL) {
          double_block = (struct indirection_block*) malloc(BLOCK_SIZE);
          ll_read(inode->double_indirection_block, 0, double_block, BLOCK_SIZE);
        }
        // Using `read` to see if this is the first loop
        if (read == 0 || double_index_shortcut != double_index) {
          // While looping we've moved to a new single block within a double.
          // Toss out of the old and load in the new block.
          single_block = (struct indirection_block*) malloc(BLOCK_SIZE);
          ll_read(double_index, 0, single_block, BLOCK_SIZE);
          double_index_shortcut = double_index;
        }
        absolute_block = single_block->next[single_index];
        break;
    }
    // Read an entire block or just a bit of one? Watch out for subblock offset
    int to_read = min(size - read, BLOCK_SIZE - inode_subblock_offset);
    ll_read(absolute_block, inode_subblock_offset, buffer, to_read);
    if (inode_subblock_offset != 0) { 
      // Haven't yet dealt with the sub block offset. This only happens once!
      PRINT("Read was a sub block offset: %d bytes", inode_subblock_offset);
      inode_subblock_offset = 0;
    }
    read += to_read;
    inode_block++;
  }
  // End while loop. Do cleanup
  if (single_block != NULL) {
    free(single_block);
  }
  if (double_block != NULL) {
    free(double_block);
  }
  return read;
}

// Write `size` data to inode from `offset`. Returns the bytes written or -1
int inode_write_data(struct inode* inode, int offset, char* buffer, int size) {
  // If they ask for an offset that produces holes
  if (offset > inode->size) {
    PRINT("BAD: Asked for an offset that produces holes in the file");
    return -1;
  }
  int inode_block = offset / BLOCK_SIZE;
  int inode_subblock_offset = offset % BLOCK_SIZE;
  // Number of bytes written
  int written = 0;
  // Relative blocks are in the inode. Absolute is the entire disk/segment
  u16 absolute_block;
  u8 direct_index, single_index, double_index;
  // These indirection blocks aren't used the same way as in `inode_read_data`
  struct indirection_block* single_block = NULL;
  struct indirection_block* double_block = NULL;
  struct indirection_index_map* double_singles[sizeof(double_block->next)];
  int len_double_singles = 0;
  int sz_indirection_map = sizeof(struct indirection_index_map);

  // Ideally reaches `size` but limited maximum number of blocks per inode
  while (written < size && inode_block < 10 + 256 + (256 * 256)) {
    // Save where the block will be written
    absolute_block = absolute_memory_segment_block();

    // Handle case where there's a provided offset (ugh, but only happens once)
    // Write an entire block or just a bit of one? If a bit, persist the old
    int to_write = min(size - written, BLOCK_SIZE - inode_subblock_offset);

    if (to_write < BLOCK_SIZE) {
      PRINT("Writing %d bytes. Need to pad with a full block", to_write);
      // Read data block. Write from subblock offset up to one block.

      // Can't allocate this into the segment since it might auto-write to disk
      char* prev_block = (char*) malloc(BLOCK_SIZE);
      int read_offset = inode_block * BLOCK_SIZE;
      int read = inode_read_data(inode, read_offset, prev_block, BLOCK_SIZE);
      if (read == -1) {
        // That's not a valid read, which is fine
        memset(prev_block, 0, BLOCK_SIZE);
      } else if (read != BLOCK_SIZE) {
        PRINT("BAD ASSERT: Read < BLOCK_SIZE from inode. Only %d bytes", read);
        exit(EXIT_FAILURE);
      }
      // Write from the offset to the end of the block onto prev_block
      char* prev_block_wh = prev_block + inode_subblock_offset;
      memcpy(prev_block_wh, buffer + written, to_write);
      ll_write_block(prev_block);
      
      // FIXME: I'm not sure why, and I've spend some time in the debugger about
      // it to ensure the addresses stay the same, but this throws saying it's
      // invalid pointer even though it's 0x555555667a0 at malloc and at free
      // free(prev_block);

      if (inode_subblock_offset != 0) {
        // This happens on the first loop, so assert that to be true
        if (written != 0) {
          PRINT("BAD ASSERT: Inode subblock offset after writing");
          return -1;
        }
        inode_subblock_offset = 0;
      }
    } else {
      if (inode_subblock_offset != 0) {
        PRINT("BAD ASSERT: Inode subblock offset not 0 for normal write");
        return -1;
      }
      ll_write_block(buffer + written);
    }
    // Now that block is written, update the inode to point to it
    // Note the wh was moved by ll_write_block which is why it was saved before
    enum index_block lookup_type =
      inode_block_to_indirection_indices(
        inode_block, &direct_index, &single_index, &double_index);
    
    switch(lookup_type) {
      case B_DIRECT:
        // TODO: Unallocate block from in memory FBV
        inode->blocks[direct_index] = absolute_block;
        break;
      case B_SINGLE:
        if (single_block == NULL) {
          single_block = (struct indirection_block*) malloc(BLOCK_SIZE);
        }
        if (inode->single_indirection_block != 0) {
          // TODO: Unallocate block from in memory FBV
          ll_read(inode->single_indirection_block, 0, single_block, BLOCK_SIZE);
        } else {
          // TODO: Is this even needed? Should be consistent with mallocs...
          memset(single_block, 0, BLOCK_SIZE);
        }
        single_block->next[single_index] = absolute_block;
        break;
      case B_DOUBLE:
        if (double_block == NULL) {
          double_block = (struct indirection_block*) malloc(BLOCK_SIZE);
        }
        if(inode->double_indirection_block != 0) {
          // TODO: Unallocate block from in memory FBV
          ll_read(inode->double_indirection_block, 0, double_block, BLOCK_SIZE);
        } else {
          // TODO: Is this even needed? Should be consistent with mallocs...
          memset(double_block, 0, BLOCK_SIZE);
        }
        struct indirection_index_map* ds;
        for (int i = 0; i < len_double_singles; i++) {
          if (double_singles[i]->index == double_index) {
            ds = double_singles[i];
            goto single_allocated;
          }
        }
        // Else, not found so set it up
        ds = double_singles[len_double_singles];
        ds = (struct indirection_index_map*) malloc(sz_indirection_map);
        ds->single = (struct indirection_block*) malloc(BLOCK_SIZE);
        ds->index = double_index;
        len_double_singles++;

        if (double_block->next[double_index] != 0) {
          // TODO: Unallocate block from in memory FBV
          ll_read(double_block->next[double_index], 0, ds->single, BLOCK_SIZE);
        } else {
          memset(ds->single, 0, BLOCK_SIZE);
        }
single_allocated:
        ds->single->next[single_index] = absolute_block;
        break;
    }
    written += to_write;
    inode_block++;
  }
  // End while loop. Write indirections and cleanup
  if (double_block != NULL) {
    for (int i = 0; i < len_double_singles; i++) {
      if (double_singles[i] != NULL) {
        absolute_block = absolute_memory_segment_block();
        ll_write_block(double_singles[i]->single->next);
        double_block->next[double_singles[i]->index] = absolute_block;
        free(double_singles[i]->single);
        free(double_singles[i]);
      }
    }
    absolute_block = absolute_memory_segment_block();
    ll_write_block(double_block);
    inode->double_indirection_block = absolute_block;
    free(double_block);
  }
  if (single_block != NULL) {
    absolute_block = absolute_memory_segment_block();
    ll_write_block(single_block);
    inode->single_indirection_block = absolute_block;
    free(single_block);
  }
  inode->size = offset + written;
  ll_write_inode(inode);
  return written;
}

// Not to be implemented. Read documentation section on future work:
// int inode_append_data(struct inode* inode, char* buffer, int size) {}
// int inode_truncate_data(struct inode* inode, int to_size) {}

// Walk a directory for a name and return its inode ID
int directory_name_to_inode_id(struct inode* directory, char* name) {
  int entries = directory->size / sizeof(struct directory_entry);
  // Used to malloc(BLOCK_SIZE) but had issues... This is less efficient/less
  // nice for memory use, but it works
  struct directory_entry directory_buffer[entries];

  // If there's not even one entry
  if (directory->size < sizeof(struct directory_entry)) {
    if (directory->size != 0) {
      PRINT("BAD ASSERT: Directory doesn't hold an entry but isn't zero");
    }
    return -1;
  }
  inode_read_data(directory, 0, (char*) directory_buffer, directory->size);
  for (int i = 0; i < entries; i++) {
    // TODO: Would strncmp be better? To sizeof(buffer->filename)?
    if (strcmp((&directory_buffer[i])->filename, name) == 0) {
      return (&directory_buffer[i])->inode;
    }
  }
  return -1;
}

// Turn a path into an inode ID. Returns the ID or -1
int resolve_path_to_inode_id(char* path) {
  char delimiter = '/';
  if (path[0] != delimiter) {
    PRINT("Path not absolute");
    return -1;
  }
  // Read the inode
  struct inode* inode_buffer = (struct inode*) malloc(sizeof(struct inode));
  ll_load_inode(ll->root_inode_id, inode_buffer);
  HEXDUMP("Reread root inode", inode_buffer, sizeof(struct inode));

  // TODO: This assert should be checked for any map to real inode translation
  if (inode_buffer->id != ll->root_inode_id) {
    PRINT("BAD ASSERT: Root inode ID isn't ll->root_inode_id");
    exit(EXIT_FAILURE);
  }
  if (inode_buffer->flags != T_DIRECTORY) {
    PRINT("BAD ASSERT: Root inode isn't a directory");
    exit(EXIT_FAILURE);
  }
  int inode_id = -1;
  int inode_block = -1;
  if (strcmp(path, "/") == 0) {
    inode_id = ll->root_inode_id;
    goto leave;
  }
  // Starts at the root directory, since that's what's loaded in already
  struct inode* current_inode = inode_buffer;
  // POSIX `basename()` is...unusual and not how I consider paths

  // You'll get a segfault with strtok if `path` is constant. They say to use
  // the strdup() method but it's not on my system...
  char* token = strtok(path, &delimiter);
  while(token != NULL) {
    PRINT("TOKEN: %s", token);
    inode_id = directory_name_to_inode_id(current_inode, token);
    if (inode_id == -1) {
      PRINT("No such file or directory, %s", token);
      goto leave;
    }
    // Otherwise follow the inode ID to a block
    inode_block = ll->imap[inode_id];
    if (inode_block == -1) {
      PRINT("BAD ASSERT: Directory points to inode not in inode map");
      exit(EXIT_FAILURE);
    }
    ll_read(inode_block, 0, inode_buffer, BLOCK_SIZE);

    // Next token
    token = strtok(NULL, &delimiter);
  }
  // If we made it this far, `inode_id` is valid
leave:
  // For linux.csc only - Unfortunate and not understood why this fails
  // free(inode_buffer);
  return inode_id;
}

// Append to a directory entry. Returns 0 or -1
int directory_add(struct inode* directory, struct directory_entry* entry) {
  if (directory->flags != T_DIRECTORY) {
    PRINT("Inode ID %d is not a directory", directory->id);
    return -1;
  }
  char* name = entry->filename;
  if (strpbrk(name, "/") != 0) {
    PRINT("Name '%s' has a /. Refusing to add", name);
    return -1;
  }
  if (directory_name_to_inode_id(directory, name) != -1) {
    PRINT("Name already exists in that parent directory");
    return -1;
  }
  int wrote = inode_write_data(
    directory, directory->size, (char*) entry, sizeof(struct directory_entry));
  if (wrote == -1 || wrote != sizeof(struct directory_entry)) {
    PRINT("Wasn't able to add entry to directory");
    return -1;
  }
  return 0;
}
