#pragma once

// Useful methods found on StackOverflow for debugging, including a runtime
// version of hexdump to analyze structs

// This isn't mine, but I can't find the links anymore

#ifndef NPRINT
# include <stdio.h>
# include <errno.h>
# define PRINT(format, ...)                               \
  ({                                                      \
    int errno_save = errno;                               \
    printf("%s:%u %s(): ", __FILE__, __LINE__, __func__); \
    printf(format, ## __VA_ARGS__);                       \
    putc('\n', stdout);                                   \
    errno = errno_save;                                   \
  })
#else // ifndef NPRINT
# define PRINT(format, ...)
#endif // ifndef NPRINT

// I actually modified this a lot to skip rows of 00, but unfortunately it uses
// carriage returns and backspaces so writing the output to a file is awful.

// For this project I write to the terminal so it's OK but I wouldn't recommend
// it for anything else. Ideally it should be rewritten to store in a buffer
// using sprintf() rather than printf() and then decide to write or not later,
// not as a stream

void HEXDUMP (char *desc, void *addr, int len) {
  int i;
  int allzero = 0;
  unsigned char buff[17];
  unsigned char *pc = (unsigned char*)addr;

  // Output description if given.
  if (desc != NULL) printf("%s:\n", desc);

  if (len == 0) {
    printf("  ZERO LENGTH\n");
    return;
  }
  if (len < 0) {
    printf("  NEGATIVE LENGTH: %i\n",len);
    return;
  }
  // Process every byte in the data.
  for (i = 0; i < len; i++) {
    // Multiple of 16 means new line (with line offset).
    if ((i % 16) == 0) {
      // Just don't print ASCII for the zeroth line.
      if (i != 0) {
        // Is it worth printing the line at all?
        if (allzero > 0) {
          if (allzero == 1) {
            // The first time a line has been skipped
            // Back up and write that it's just zeros
            for (int j = 0; j < 11; j++) {
              printf("\b");
            }
            printf(" ... ZEROES\n  ...\n");
            allzero = 2;
          } else {
            printf("\r");
          }
        } else {
          allzero = 1;
          printf("  %s\n", buff);
        }
      }
      // Output the offset.
      printf("  %04x ", i);
      printf("  %05d ", i);
    }
    // Now the hex code for the specific character.
    if (pc[i] != 0x00) {
      allzero = 0;
    }
    printf(" %02x", pc[i]);

    // And store a printable ASCII character for later.
    if ((pc[i] < 0x20) || (pc[i] > 0x7e)) {
      buff[i % 16] = '.';
    } else {
      buff[i % 16] = pc[i];
    }
    buff[(i % 16) + 1] = '\0';
  }
  // Pad out last line if not exactly 16 characters.
  while ((i % 16) != 0) {
    printf("   ");
    i++;
  }

  // And print the final ASCII bit.
  printf("  %s\n", buff);
  fflush(stdout);
}
