#pragma once

// From https://stackoverflow.com/questions/1225998/bitmap-in-c
// Surprisingly difficult to have a bitmap in C

#include <limits.h> // For CHAR_BIT
#include <stdint.h> // For uint32_t

// 512 bytes * 8 bits/byte = 4096 bits. 32 bits/int = 128 ints
enum { BITS_PER_WORD = sizeof(uint32_t) * CHAR_BIT };
#define WORD_OFFSET(b) ((b) / BITS_PER_WORD)
#define BIT_OFFSET(b)  ((b) % BITS_PER_WORD)

void set_bit(uint32_t *words, int n) {
  words[WORD_OFFSET(n)] |= (1 << BIT_OFFSET(n));
}

void clear_bit(uint32_t *words, int n) {
  words[WORD_OFFSET(n)] &= ~(1 << BIT_OFFSET(n));
}

int get_bit(uint32_t *words, int n) {
  uint32_t bit = words[WORD_OFFSET(n)] & (1 << BIT_OFFSET(n));
  return bit != 0;
}