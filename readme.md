# LLFS - Filesystem assignment

The _Little Log Filesystem_. This is a log structured filesystem that is
designed for write performance by buffering all modifications into a buffer and
writing them in segments. The disk is append only and uses a cleaner to reuse
segments and handle defragmentation.

This assignment includes a specification document, but there's no requirement
for it to be followed; they're just ideas. Since there's a lot of flexibility,
I'm _planning_ to implement multiuser filesystem level encryption. This isn't
that hard compared to the log structure itself.

The filesystem will use FUSE to be mounted and used by other applications.

FUSE development headers aren't installed on _linux.csc_, the testing server,
but there is FUSE 2.9 installed to mount precompiled binaries. It'll be tricky.

## TODO

Also see the list of supported interface methods below...

Implement open/close and convert operations to use inodes. They're going to be
used internally by FUSE anyway via `fi->fh`.

Remove some oddities like inode blocks being shadowed by 0x77 to make them
appear in a hexdump.

Implement the cleaner. Use two in-memory FBVs and one on FBV to achieve the
transactional algorithm discussed in _writing-segments.md_. Many `TODO:`
comments are about either marking blocks or commiting versions of FBVs...
They're largely unimplemented for now.

If ever scaling beyond 2MB disk size, the N_BLOCKS_FBV needs to be looked at
again. It's not ideal right now, and scales poorly. Need to instead track
segments and their available blocks, rather than just blocks.

For key management, you can add duplicate keys. Which is awful since only the
first one will ever be found.

Find an AES-128-GCM library and support encryption. This ties into the inode's
IV field and the key management.

Inodes take up an entire block just to keep it simple, but limitations on the
inode map reduce the number of possible inodes while requiring the stored block
offset to be a u16 (or higher) - In a 2MB FS there are only 4096 blocks so most
of hat u16 is unused (65536 - 4069). To pack multiple inodes into one block, the
map could make better use of the u16 by having it refer to slots and translating
that to a block and offset.

Inode IDs aren't currently recycled, at all. Will need a method to do this and
then convert other code to now use `ll->inode_count++` as an identifier.

Similar to how segments are recycled (do a search for `next_avail_segment`),
there should be a read head that tracks the next available inode ID and it can
loop back to 0 and hop around to fill in gaps as inodes are deleted. It would be
ideal to factor out this _"next available"_ algorithm since there's so much
overlap; one uses the FBV and the other uses the inode map.

## Glossary

In the codebase I use _fbv_ to mean the free block vector, and _rh_ or _wh_ to
mean read and write heads, respectively. I think that's it. I try to make
symbol names as meaningful as possible.

## Design decisions and tradeoffs

These tradeoffs keep the code simple. That being said, I've made an attempt to
keep the design reasonably scalable. I want this to be something I use.

These requirements and tradeoffs are all related and limit each other. I need
the disk to store a reasonable amount of data. The specification's maximum disk
size of 2MB isn't very useful. Around 1GB or more is better.

To keep it simple and standard, the maximum number of inodes, along with the
block size, segment size, and disk size are all set and fixed with the creation
of a disk. I also want to keep the fixed size free block vector (bitmap) from
the original specification since it's used for cleaning/defrag.

An inode isn't very large, but I still want it to take an entire block. The
wasted space is very little, and there's _a lot_ of complexity added by trying
save every byte. I have yet to see an LFS do it. Most slides and papers brush
over the topic, and implementations just use 1 inode per block. The rest of the
space could go towards metadata, which is still useful.

Log filesystems need to append a map of inodes "periodically" - it doesn't need
to be on every segment write. Yet, to keep it simple while avoiding threads and
disk corruption problems, I want to include the map as part of each segment.

Like inodes, I want the map to be a predictable size, or at least, have a
reasonable maximum size that will always fit well in a segment and in memory.
This likely means it will be fixed. _TODO: Not sure yet_

Addressing blocks absolutely is still a good idea from the spec. This plays into
to the inode map, since choosing between an unsigned 16 bit or 32 bit number
changes the maximum map size significantly. The map is between inode ID and
block offset, where limiting the maximum ID, and therefore the maximum number of
inodes, also defines the inode map. Since it's stored in each segment, it
shouldn't be too big - imagine if 50% of each segment was a map. That'd suck.
Tweaking other parameters could ensure it uses, say, 2 blocks of a 20 block
segment.

There's no caching at all.

If the drive needs to be unmounted with a segment in memory, it is written to
disk immediately and _not restored next startup_. This could be a good idea, but
I wanted to test defragmentation via the cleaner, and this works for that.

FUSE has no notion of relative paths (bless) so all interface calls are
translated to absolute paths before they hit this library. That's great. So
there's no notion in LLFS of relativity or `.` and `..`.

## Quick notes

  - Likely use 512 bytes for a block but try to keep it flexible.
  - Looked into AES-128 encryption. Uses a 16 byte block size. Provides random
    access read and write. Inodes store a 16 byte IV for security. Encrypted
    content is only 32 bytes larger than original: `enc = ((data/16)+1)*16+IV`.
  - AES-GCM provides _authentication_ for data integrity.
  - Superblock has lots of room. SHA1 hashes of symmetric keys can be stored
    there and map to a root inode.
  - Compression via zlib wouldn't be too much code. Library files are installed
    on the test server, linux.csc.uvic.ca, via `zlib1g-devel`.

## Layout

On disk:

  - **Superblock** (info, pointers, and hashes): Probably 1 block.
  - **Free block vector (FBV)**: 1 bit per block so it depends on the vdisk
    size. For example, a 2MB disk with 512 byte blocks need a 4096 bitmap which
    fits in 1 block since 512 bytes * 8 bits is also 4096. At 1 FBV block per
    4096 data blocks, a 100MB vdisk would need 50 FBV blocks.
  - **Log**: This is the rest of the filesystem. It's append-only and written to
    in segments. Inodes, their data blocks, and the inode map are all in here.
    Segments are recycled and defragmented at certain thresholds.

In memory:

  - **ll->** global LLFS object with information and pointers, including:
  - **Segment** the buffer that will be written to disk
  - **Inode map** is stored always in the last two blocks of the segment

## Testing disk sizes and scalability

This is mostly about choosing between 8, 16, or 32 bit integers for storing
data. How large do you want your disk to be? How many files will it store?

If you choose u8 for inodes, then you can only have 255 files or directories.
Meanwhile, a u16 will land you 65536, but you'd never need that many if your
vdisk size was only 2MB, like the spec is.

Using a u16 for block addressing means the maximum vdisk size is 32MB. Bit small
for modern uses. A u32 pushes it to 2GB.

Need to configure these values and try things out. A u16 for inode IDs and u32
for block addressing means the FBV would need to store somewhere around 4
billion data blocks, which works out to:

2^32 bits / (8 bits per byte * 512 bytes per block) = 1048576 blocks = 512MB.
That's a 1/4 of the disk, which means it _doesn't_ actually need to address
nearly that many data blocks since the space is shared.

Plus it's not exactly something you want to store on disk or in memory...

The free block vector should really be an available segment map which ties
segment number to number of blocks used.

TODO:^^^

## API

I don't think I'll support `open()` and `close()`. FUSE does everything in full
absolute paths I believe, so I shouldn't need to support file pointers.

_TODO: These will change when they need to adapt FUSE conventions_

  - [x] `stat (path, stat_buf)`: Inodes don't store much metadata but this could
    read from the empty space behind an inode instead
  - [x] `access (path)`
  - [ ] `readdir (path)`: FUSE makes this very interesting; read below.
  - [ ] `mkdir (path)`
  - [ ] `unlink (path)`
  - [ ] `rename (from, to)`
  - [ ] `rmdir (path)`
  - [x] `create (path)`
  - [x] `read (path, buffer, size, offset)`
  - [x] `write (path, buffer, size, offset)`

Not supporting:

  - symlink (from, to)
  - readlink (path, buffer, size)
  - link (from, to)
  - open (path)
  - close (fp)

The open() call would be useful to avoid translating the path to an inode every
time. This could also be done with a cache.

### FUSE

Here are some little notes on FUSE.

This _hello_ example shows a very basic filesystem that only has one file in the
root directory, and you specify its name and content at mount time.

- http://libfuse.github.io/doxygen/hello_8c.html

The official _passthrough_ example shows all the functions FUSE can support and
what parameters they need:

- https://github.com/libfuse/libfuse/blob/master/example/passthrough.c

Reading from a directory is interesting. The caller must allocate a buffer for
the directory entries, but FUSE handles this, and paging, by providing a
method which eventually returns 1 when the buffer is full. It will call readdir
again, later, with an `offset` to indicate where it wants the FS to pick back up
the listing.

That's great because it means I don't need to allocate or return a buffer.

- https://stackoverflow.com/questions/23565151/fuse-setting-offsets-for-the-filler-function-in-readdir
- https://stackoverflow.com/questions/32024674/change-buffer-size-in-fuse-readdir
- http://fuse.996288.n3.nabble.com/issues-in-readdir-and-fuse-fill-dir-t-filler-td10397.html

### Always a path and maybe an inode

FUSE operations are always declared to accept a path as their first argument.
This is absolute (bless). However, they also accept an `fi->` object of these
properties:

From https://libfuse.github.io/doxygen/structfuse__file__info.html

```
int 	flags
unsigned int 	writepage: 1
unsigned int 	direct_io: 1
unsigned int 	keep_cache: 1
unsigned int 	flush: 1
unsigned int 	nonseekable: 1
unsigned int 	padding: 27
uint64_t 	fh
uint64_t 	lock_owner
uint32_t 	poll_events
```

The `fh` and `flags` are the only members of importance right now. File handle
is the inode set by an open or create call. This lets you not translate the path
into an inode, and save some time - as far as I can see. For flags, only
O_APPEND seems of interest to save data blocks and segments.

### Operations, workarounds, and _"Not implemented"_

FUSE defines an interface for a filesystem to tap into around 34 functions such
as rename, link, mkdir, etc.

Here's an example write call that overwrites a file. FUSE translates into a
series of supported operations:

From https://sourceforge.net/p/fuse/mailman/message/23641142/

```
$ echo "xxx" > tmp/abc
  Function getattr()  called with path=/abc
  Function truncate() called with path=/abc
  Function getattr()  called with path=/abc
  Function open()     called with path=/abc
  Function flush()    called with path=/abc
  Function write()    called with path=/abc
  Function flush()    called with path=/abc
  Function release()  called with path=/abc
```

Implementing all the operations would be a lot of work, but a minimal filesystem
can operate with only 3-4 of these calls. If an operation is requested that the
FS doesn't support, it tries to use supported calls to work around - for
instance, if no `truncate()` is provided to clear a file, it can unlink and
create a new file. When there's no alternative, FUSE returns an error to the
caller like "Read-only filesystem" or "Function not implemented: truncate".

This works very well and allows LLFS to be implemented with fewer operations.

See the ["Future work"](#future-work) section where the order of operations are
discussed and their tradeoffs of not being implemented.

## File types

There are only files and directories. Before 6e9d3e05 I considered hardlinks and
symlinks. The commit has some notes.

## Future work

These are neat ideas but there's no time for them in this project,
unfortunately.

### Implementing beyond FUSE minimum API calls

Append is easy, and shouldn't even be part of the library. Just set the write
offset to inode->size. Truncate is _not_ easy for any other number than 0 since
dealing with inode blocks and sub block sizing hurts to think about.

FUSE doesn't need these to run, so they're on hold or won't be implemented.

- Shells use O_APPEND for `>>` operator:
  https://unix.stackexchange.com/questions/202797/does-bash-o-append
- Append supported by FUSE via flags:
  https://sourceforge.net/p/fuse/mailman/message/35272065/

### Sharing via asymmetric encryption

Had an idea of sharing files by encrypting content with a document key, and then
encrypting the priavte key of the document with each users public key if they
were to have access to the file. The file would also need to be linked into
their directories, somehow.

The superblock would hold a public key to inode mapping, similar to the SHA1
hash mapping that exists today.

It's too much work to make this happen, and unfortunately that mostly comes down
to the interface: Desktop file manager aren't expecting any "sharing" options,
and FUSE will not either. There's no way to make this accessible.

- Asymmetric keys (public-private) generally make sharing possible, and that's
  large to store inside the FS. How to address people? How to let someone know
  what they have access to?

- Revocable sharing is hard

  - Either duplicate and re-encrypt content for each shared user (doesn't scale)
    so it can be deleted safely, or

  - Else encrypt content once, then encrypt the document encryption key for each
    user's public key so a revocation is only a re-encrypt of the content once
    and all keys

  - It's a lot of work

- Support access via OpenGPG? This is hard to tap into as a C program and there
  doesn't seem to be great documentation on how it's done.

Could also implement anonymous sharing by *not* creating a root directory for a
public key, but providing an anonymous public key, so 0000000, where it's unsure
who can decrypt it. Someone can. It's waiting for them. Everyone is able to try,
but it will likely fail.
